# jeebbsV4.0

## 简介

本项目集成

jeebbsV4.0功能列表
1、论坛APP
2、登录更改shiro登录认证以及记住我
3、在线人数、时长统计
4、提供用户接口、其他系统用户接口调用设置以及接口管理（可与jeecms系列软件无缝对接实现单点登录）
5、用户自定义字段
6、禁用ip、id发帖、回帖
7、注册成功自动登录
8、设置在线活跃度等级
9、手机模板方案设置
10、最近登录过（三天，一周、一个月、三个月、半年）查询
11、类似微信团队号（与用户沟通账户以及推送系统消息）
12、QQ登录
jeebbsV4.0修复以及完善部分
1.权限的访问的地址链接http://bbs.jeecms.com/xtjc/31406.jhtml
2.图片太大显示不全问题
3.会员组设置附件上线没有用以及其他相关设置无效
4.附件上传经常上传不了
5.发帖文字内容不能居中、居左、居右 编辑器字体、大小、插入图片、排序列表无效、左浮动、右浮动
6.用户自定义头像错误(ttp://bbs.jeecms.com/member/editUserImg.jhtml和http://bbs.jeecms.com/member/update.jspx)
7.注册如果发送邮件激活的方式出错（返回页面错误org.hibernate.LazyInitializationException: could not initialize proxy - no Session）
8.禁止用户后不允许登录、发帖、回帖等
9.后台会员搜索中文名搜索乱码
10.注册会员的时候提示hq-pmis@chinaunicom.cn这个邮箱的格式不对
11.登录设置邮箱密码文本框改成密码框

## 安装说明 
## 欢迎交流
QQ:702009189
邮箱: 702009189@qq.com