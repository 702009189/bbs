/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.18 : Database - jeebbs4f
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
-- CREATE DATABASE /*!32312 IF NOT EXISTS*/`jeebbs4f` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `jeebbs4f`;

/*Table structure for table `attachment` */

DROP TABLE IF EXISTS `attachment`;

CREATE TABLE `attachment` (
  `attachment_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `file_path` varchar(100) DEFAULT NULL COMMENT '路径',
  `file_name` varchar(100) DEFAULT NULL COMMENT '文件名称',
  `file_size` int(11) DEFAULT NULL COMMENT '大小',
  `is_pictrue` tinyint(1) DEFAULT '0' COMMENT '是否是图片',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`attachment_id`),
  KEY `FK_attachment_post` (`post_id`),
  CONSTRAINT `FK_attachment_post` FOREIGN KEY (`post_id`) REFERENCES `bbs_post` (`POST_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `attachment` */

insert  into `attachment`(`attachment_id`,`post_id`,`name`,`description`,`file_path`,`file_name`,`file_size`,`is_pictrue`,`create_time`) values (1,190,'177-1509111F528.png',NULL,'/u/cms/www/201704/28100413d541.png','177-1509111F528.png',200127,1,'2017-04-28 10:04:13');

/*Table structure for table `bbs_accredit` */

DROP TABLE IF EXISTS `bbs_accredit`;

CREATE TABLE `bbs_accredit` (
  `accredit_id` int(11) NOT NULL AUTO_INCREMENT,
  `corporation_name` varchar(100) NOT NULL DEFAULT '',
  `telphone` varchar(20) NOT NULL DEFAULT '',
  `website_name` varchar(50) NOT NULL DEFAULT '',
  `realm_name` varchar(50) NOT NULL DEFAULT '',
  `buy_time` varchar(255) NOT NULL DEFAULT '',
  `stop_time` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`accredit_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `bbs_accredit_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `jb_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='授权表';

/*Data for the table `bbs_accredit` */

/*Table structure for table `bbs_category` */

DROP TABLE IF EXISTS `bbs_category`;

CREATE TABLE `bbs_category` (
  `CATEGORY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` int(11) NOT NULL,
  `PATH` varchar(20) NOT NULL COMMENT '访问路径',
  `TITLE` varchar(100) NOT NULL COMMENT '标题',
  `PRIORITY` int(11) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  `FORUM_COLS` int(11) NOT NULL DEFAULT '1' COMMENT '板块列数',
  `moderators` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`CATEGORY_ID`),
  KEY `FK_BBS_CTG_SITE` (`SITE_ID`),
  CONSTRAINT `FK_BBS_CTG_SITE` FOREIGN KEY (`SITE_ID`) REFERENCES `jc_site` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='论坛分区';

/*Data for the table `bbs_category` */

insert  into `bbs_category`(`CATEGORY_ID`,`SITE_ID`,`PATH`,`TITLE`,`PRIORITY`,`FORUM_COLS`,`moderators`) values (2,1,'dzjj','电子竞技',0,1,'ss'),(3,1,'dsyx','电视游戏',0,1,'sd');

/*Table structure for table `bbs_category_user` */

DROP TABLE IF EXISTS `bbs_category_user`;

CREATE TABLE `bbs_category_user` (
  `CATEGORY_ID` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`CATEGORY_ID`,`user_id`),
  KEY `FK_BBS_CATEGORY_TO_USER` (`user_id`),
  CONSTRAINT `FK_BBS_CATEGORY_TO_USER` FOREIGN KEY (`user_id`) REFERENCES `jb_user` (`user_id`),
  CONSTRAINT `FK_BBS_USER_TO_CATEGORY` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `bbs_category` (`CATEGORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分区版主';

/*Data for the table `bbs_category_user` */

/*Table structure for table `bbs_common_magic` */

DROP TABLE IF EXISTS `bbs_common_magic`;

CREATE TABLE `bbs_common_magic` (
  `magicid` smallint(6) NOT NULL AUTO_INCREMENT COMMENT '道具id',
  `available` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否可用',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `identifier` varchar(40) NOT NULL COMMENT '唯一标识',
  `description` varchar(255) NOT NULL COMMENT '描述',
  `displayorder` tinyint(3) NOT NULL DEFAULT '0' COMMENT '顺序',
  `credit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '使用的积分',
  `price` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '价格',
  `num` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '数量',
  `salevolume` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '销售量',
  `supplytype` tinyint(1) NOT NULL DEFAULT '0' COMMENT '自动补货类型',
  `supplynum` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '自动补货数量',
  `useperoid` tinyint(1) NOT NULL DEFAULT '0' COMMENT '使用周期',
  `usenum` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '周期使用数量',
  `weight` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '重量',
  `useevent` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:只在特定环境使用 1:可以在道具中心使用',
  PRIMARY KEY (`magicid`),
  UNIQUE KEY `identifier` (`identifier`),
  KEY `displayorder` (`available`,`displayorder`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='道具数据表';

/*Data for the table `bbs_common_magic` */

insert  into `bbs_common_magic`(`magicid`,`available`,`name`,`identifier`,`description`,`displayorder`,`credit`,`price`,`num`,`salevolume`,`supplytype`,`supplynum`,`useperoid`,`usenum`,`weight`,`useevent`) values (1,1,'喧嚣卡','open','可以将主题开启，可以回复',0,1,10,0,0,2,0,2,0,10,0),(2,1,'悔悟卡','repent','可以删除自己的帖子',0,2,10,2,0,1,0,1,0,10,0),(3,1,'照妖镜','namepost','可以查看一次匿名用户的真实身份。',0,1,10,1,0,1,0,1,0,10,0),(4,1,'金钱卡','money','可以随机获得一些金币',0,2,10,44,0,1,0,1,0,10,1),(5,1,'千斤顶','jack','可以将主题顶起一段时间，重复使用可延长帖子被顶起的时间',0,1,10,0,0,0,0,0,0,10,0),(6,1,'窥视卡','showip','可以查看指定用户的 IP',0,1,10,1,0,1,0,1,0,10,1),(7,1,'抢沙发','sofa','可以抢夺指定主题的沙发',0,1,10,0,0,0,0,0,0,10,0),(8,1,'置顶卡','stick','可以将主题置顶',0,1,10,0,0,0,0,0,0,10,0),(9,1,'变色卡','highlight','可以将帖子或日志的标题高亮，变更颜色',0,1,10,2,0,0,0,0,0,10,0),(10,1,'雷达卡','checkonline','查看某个用户是否在线',0,1,10,1,0,1,0,1,0,10,1),(11,1,'沉默卡','close','可以将主题关闭，禁止回复',0,1,10,2,100,1,0,1,2,10,0),(12,1,'提升卡','bump','可以提升某个主题',0,1,10,0,0,1,0,1,0,10,0),(13,1,'匿名卡','anonymouspost','在指定的地方，让自己的名字显示为匿名。',0,1,10,0,0,0,0,0,0,10,0);

/*Table structure for table `bbs_config` */

DROP TABLE IF EXISTS `bbs_config`;

CREATE TABLE `bbs_config` (
  `CONFIG_ID` bigint(20) NOT NULL,
  `DEF_AVATAR` varchar(100) NOT NULL DEFAULT '' COMMENT '默认会员头像',
  `AVATAR_WIDTH` int(11) NOT NULL DEFAULT '160' COMMENT '头像最大宽度',
  `AVATAR_HEIGHT` int(11) NOT NULL DEFAULT '160' COMMENT '头像最大高度',
  `TOPIC_COUNT_PER_PAGE` int(11) NOT NULL DEFAULT '20' COMMENT '每页主题数',
  `POST_COUNT_PER_PAGE` int(11) NOT NULL DEFAULT '10' COMMENT '每页帖子数',
  `KEYWORDS` varchar(255) NOT NULL DEFAULT '' COMMENT '首页关键字',
  `DESCRIPTION` varchar(255) NOT NULL DEFAULT '' COMMENT '首页描述',
  `REGISTER_STATUS` smallint(6) NOT NULL DEFAULT '1' COMMENT '注册状态（0：关闭，1：开放，2：邀请）',
  `REGISTER_GROUP_ID` int(11) NOT NULL DEFAULT '1' COMMENT '注册会员组',
  `REGISTER_RULE` longtext COMMENT '注册协议',
  `CACHE_POST_TODAY` int(11) NOT NULL DEFAULT '0' COMMENT '今日贴数',
  `CACHE_POST_YESTERDAY` int(11) NOT NULL DEFAULT '0' COMMENT '昨日帖数',
  `CACHE_POST_MAX` int(11) NOT NULL DEFAULT '0' COMMENT '最高帖数',
  `CACHE_POST_MAX_DATE` date NOT NULL COMMENT '最高帖数日',
  `CACHE_TOPIC_TOTAL` int(11) NOT NULL DEFAULT '0' COMMENT '总主题',
  `CACHE_POST_TOTAL` int(11) NOT NULL DEFAULT '0' COMMENT '总帖数',
  `CACHE_USER_TOTAL` int(11) NOT NULL DEFAULT '0' COMMENT '总会员',
  `last_user_id` int(11) DEFAULT NULL COMMENT '最新会员',
  `site_id` int(11) NOT NULL,
  `DEFAULT_GROUP_ID` bigint(20) NOT NULL DEFAULT '1' COMMENT '默认会员组',
  `TOPIC_HOT_COUNT` int(11) NOT NULL DEFAULT '10' COMMENT '热帖回复数量',
  `AUTO_REGISTER` tinyint(1) DEFAULT '1' COMMENT '是否自动注册',
  `EMAIL_VALIDATE` tinyint(1) DEFAULT '0' COMMENT '开启邮箱验证',
  PRIMARY KEY (`CONFIG_ID`),
  KEY `FK_BBS_CONFIG_SITE` (`site_id`),
  CONSTRAINT `FK_BBS_CONFIG_SITE` FOREIGN KEY (`site_id`) REFERENCES `jc_site` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='论坛配置';

/*Data for the table `bbs_config` */

insert  into `bbs_config`(`CONFIG_ID`,`DEF_AVATAR`,`AVATAR_WIDTH`,`AVATAR_HEIGHT`,`TOPIC_COUNT_PER_PAGE`,`POST_COUNT_PER_PAGE`,`KEYWORDS`,`DESCRIPTION`,`REGISTER_STATUS`,`REGISTER_GROUP_ID`,`REGISTER_RULE`,`CACHE_POST_TODAY`,`CACHE_POST_YESTERDAY`,`CACHE_POST_MAX`,`CACHE_POST_MAX_DATE`,`CACHE_TOPIC_TOTAL`,`CACHE_POST_TOTAL`,`CACHE_USER_TOTAL`,`last_user_id`,`site_id`,`DEFAULT_GROUP_ID`,`TOPIC_HOT_COUNT`,`AUTO_REGISTER`,`EMAIL_VALIDATE`) values (1,'1',160,160,20,10,'xyrjBBS','xyrjBBS',1,1,'',10,0,10,'2015-03-19',1,10,3,5,1,1,0,1,0);

/*Table structure for table `bbs_config_attr` */

DROP TABLE IF EXISTS `bbs_config_attr`;

CREATE TABLE `bbs_config_attr` (
  `config_id` bigint(20) NOT NULL DEFAULT '0',
  `attr_name` varchar(30) NOT NULL COMMENT '名称',
  `attr_value` varchar(255) DEFAULT NULL COMMENT '值',
  KEY `fk_attr_config` (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='BBS配置属性表';

/*Data for the table `bbs_config_attr` */

insert  into `bbs_config_attr`(`config_id`,`attr_name`,`attr_value`) values (1,'keepMinute','15');

/*Table structure for table `bbs_credit_exchange` */

DROP TABLE IF EXISTS `bbs_credit_exchange`;

CREATE TABLE `bbs_credit_exchange` (
  `eid` int(11) NOT NULL DEFAULT '0',
  `expoint` int(11) NOT NULL DEFAULT '0' COMMENT '兑换比率积分基数',
  `exprestige` int(11) NOT NULL DEFAULT '0' COMMENT '兑换比率威望基数',
  `pointoutavailable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '积分是否可以兑出',
  `pointinavailable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '积分是否允许兑入',
  `prestigeoutavailable` tinyint(3) NOT NULL DEFAULT '0' COMMENT '威望是否允许兑出',
  `prestigeinavailable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '威望是否允许兑入',
  `exchangetax` float(2,1) NOT NULL DEFAULT '0.0' COMMENT '兑换交易税',
  `mini_balance` int(11) NOT NULL DEFAULT '0' COMMENT '兑换最低余额',
  PRIMARY KEY (`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='积分交易规则';

/*Data for the table `bbs_credit_exchange` */

insert  into `bbs_credit_exchange`(`eid`,`expoint`,`exprestige`,`pointoutavailable`,`pointinavailable`,`prestigeoutavailable`,`prestigeinavailable`,`exchangetax`,`mini_balance`) values (1,1,10,1,1,1,1,0.2,0);

/*Table structure for table `bbs_credit_rule` */

DROP TABLE IF EXISTS `bbs_credit_rule`;

CREATE TABLE `bbs_credit_rule` (
  `rid` int(11) NOT NULL AUTO_INCREMENT COMMENT '规则ID',
  `rulename` varchar(20) NOT NULL DEFAULT '' COMMENT '规则名称',
  `action` varchar(50) NOT NULL DEFAULT '' COMMENT '规则action唯一KEY',
  `cycletype` tinyint(1) NOT NULL DEFAULT '0' COMMENT '奖励周期0:一次;1:每天;2:整点;3:间隔分钟;4:不限;',
  `cycletime` int(10) NOT NULL DEFAULT '0' COMMENT '间隔时间',
  `rewardnum` tinyint(2) NOT NULL DEFAULT '1' COMMENT '奖励次数',
  `extcredits1` int(10) NOT NULL DEFAULT '0' COMMENT '扩展1',
  `extcredits2` int(10) NOT NULL DEFAULT '0' COMMENT '扩展2',
  `extcredits3` int(10) NOT NULL DEFAULT '0' COMMENT '扩展3',
  `extcredits4` int(10) NOT NULL DEFAULT '0' COMMENT '扩展4',
  `ext1name` varchar(20) DEFAULT NULL COMMENT '扩展1别名',
  `ext2name` varchar(20) DEFAULT NULL COMMENT '扩展2别名',
  `ext3name` varchar(20) DEFAULT NULL COMMENT '扩展3别名',
  `ext4name` varchar(20) DEFAULT NULL COMMENT '扩展4别名',
  `ext1avai` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用\n(0:不启用 1:启用但不显示 2:启用并显示)',
  `ext2avai` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用\n(0:不启用 1:启用但不显示 2:启用并显示)',
  `ext3avai` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用\n(0:不启用 1:启用但不显示 2:启用并显示)',
  `ext4avai` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用\n(0:不启用 1:启用但不显示 2:启用并显示)',
  `ext1exchangeout` tinyint(1) DEFAULT '0' COMMENT '积分兑出',
  `ext2exchangeout` tinyint(1) DEFAULT '0' COMMENT '积分兑出',
  `ext3exchangeout` tinyint(1) DEFAULT '0' COMMENT '积分兑出',
  `ext4exchangeout` tinyint(1) DEFAULT '0' COMMENT '积分兑出',
  `ext1exchangein` tinyint(1) DEFAULT '0' COMMENT '积分兑入',
  `ext2exchangein` tinyint(1) DEFAULT '0' COMMENT '积分兑入',
  `ext3exchangein` tinyint(1) DEFAULT '0' COMMENT '积分兑入',
  `ext4exchangein` tinyint(1) DEFAULT '0' COMMENT '积分兑入',
  `credittax` tinyint(2) DEFAULT NULL COMMENT '积分交易税',
  `minibalance` int(10) DEFAULT NULL COMMENT '兑换最低余额',
  PRIMARY KEY (`rid`),
  UNIQUE KEY `action` (`action`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='积分规则表';

/*Data for the table `bbs_credit_rule` */

/*Table structure for table `bbs_forum` */

DROP TABLE IF EXISTS `bbs_forum`;

CREATE TABLE `bbs_forum` (
  `FORUM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CATEGORY_ID` int(11) NOT NULL COMMENT '分区ID',
  `SITE_ID` int(11) NOT NULL COMMENT '站点ID',
  `POST_ID` int(11) DEFAULT NULL COMMENT '最后回帖',
  `replyer_id` int(11) DEFAULT NULL COMMENT '最后回帖会员',
  `PATH` varchar(20) NOT NULL COMMENT '访问路径',
  `TITLE` varchar(150) NOT NULL COMMENT '标题',
  `DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '描述',
  `KEYWORDS` varchar(255) DEFAULT NULL COMMENT 'meta-keywords',
  `FORUM_RULE` varchar(255) DEFAULT NULL COMMENT '版规',
  `PRIORITY` int(11) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  `TOPIC_TOTAL` int(11) NOT NULL DEFAULT '0' COMMENT '主题总数',
  `POST_TOTAL` int(11) NOT NULL DEFAULT '0' COMMENT '帖子总数',
  `POST_TODAY` int(11) NOT NULL DEFAULT '0' COMMENT '今日新帖',
  `OUTER_URL` varchar(255) DEFAULT NULL COMMENT '外部链接',
  `POINT_TOPIC` int(11) NOT NULL DEFAULT '0' COMMENT '发贴加分',
  `POINT_REPLY` int(11) NOT NULL DEFAULT '0' COMMENT '回帖加分',
  `POINT_PRIME` int(11) NOT NULL DEFAULT '0' COMMENT '精华加分',
  `LAST_TIME` datetime DEFAULT NULL COMMENT '最后发贴时间',
  `TOPIC_LOCK_LIMIT` int(11) NOT NULL DEFAULT '0' COMMENT '锁定主题（天）',
  `moderators` varchar(100) DEFAULT NULL COMMENT '版主',
  `group_views` varchar(100) DEFAULT NULL COMMENT '访问会员组',
  `group_topics` varchar(100) DEFAULT NULL COMMENT '发帖会员组',
  `group_replies` varchar(100) DEFAULT NULL COMMENT '回复会员组',
  `POINT_AVAILABLE` tinyint(1) DEFAULT NULL COMMENT '积分是否启用',
  `PRESTIGE_AVAILABLE` tinyint(1) DEFAULT NULL COMMENT '威望是否启用',
  `PRESTIGE_TOPIC` int(11) DEFAULT '0' COMMENT '发帖加威望',
  `PRESTIGE_REPLY` int(11) DEFAULT '0' COMMENT '回帖加威望',
  `PRESTIGE_PRIME1` int(11) DEFAULT '0' COMMENT '精华1加威望',
  `PRESTIGE_PRIME2` int(11) DEFAULT '0' COMMENT '精华2加威望',
  `PRESTIGE_PRIME3` int(11) DEFAULT '0' COMMENT '精华3加威望',
  `PRESTIGE_PRIME0` int(11) DEFAULT '0' COMMENT '解除精华扣除威望',
  PRIMARY KEY (`FORUM_ID`),
  KEY `FK_BBS_FORUM_CTG` (`CATEGORY_ID`),
  KEY `FK_BBS_FORUM_USER` (`replyer_id`),
  KEY `FK_BBS_FORUM_POST` (`POST_ID`),
  KEY `FK_BBS_FORUM_WEBSITE` (`SITE_ID`),
  CONSTRAINT `FK_BBS_FORUM_CTG` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `bbs_category` (`CATEGORY_ID`),
  CONSTRAINT `FK_BBS_FORUM_POST` FOREIGN KEY (`POST_ID`) REFERENCES `bbs_post` (`POST_ID`),
  CONSTRAINT `FK_BBS_FORUM_USER` FOREIGN KEY (`replyer_id`) REFERENCES `jb_user` (`user_id`),
  CONSTRAINT `FK_BBS_FORUM_WEBSITE` FOREIGN KEY (`SITE_ID`) REFERENCES `jc_site` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='论坛板块';

/*Data for the table `bbs_forum` */

insert  into `bbs_forum`(`FORUM_ID`,`CATEGORY_ID`,`SITE_ID`,`POST_ID`,`replyer_id`,`PATH`,`TITLE`,`DESCRIPTION`,`KEYWORDS`,`FORUM_RULE`,`PRIORITY`,`TOPIC_TOTAL`,`POST_TOTAL`,`POST_TODAY`,`OUTER_URL`,`POINT_TOPIC`,`POINT_REPLY`,`POINT_PRIME`,`LAST_TIME`,`TOPIC_LOCK_LIMIT`,`moderators`,`group_views`,`group_topics`,`group_replies`,`POINT_AVAILABLE`,`PRESTIGE_AVAILABLE`,`PRESTIGE_TOPIC`,`PRESTIGE_REPLY`,`PRESTIGE_PRIME1`,`PRESTIGE_PRIME2`,`PRESTIGE_PRIME3`,`PRESTIGE_PRIME0`) values (3,2,1,189,29,'yylm','LOL英雄联盟','','lol,联盟','',1,2,2,2,'',5,0,100,'2017-04-27 17:12:43',0,'702009189',',14,1,2,3,4,5,6,7,8,9,10,11,12,13,',',1,2,3,4,5,6,7,8,9,10,11,12,13,',',1,2,3,4,5,6,7,8,9,10,11,12,13,',1,1,1,0,1,2,3,0),(4,2,1,191,35,'tymyd','天涯明月刀','','天涯,明月','',2,1,2,2,'',5,0,100,'2017-04-28 11:27:44',1,'',',14,1,2,3,4,5,6,7,8,9,10,11,12,13,',',1,2,3,4,5,6,7,8,9,10,11,12,13,',',1,2,3,4,5,6,7,8,9,10,11,12,13,',1,1,1,0,1,2,3,0),(5,3,1,NULL,NULL,'ps3','ps3','','ps3','',3,0,0,0,'',5,0,100,NULL,0,'',',14,1,2,3,4,5,6,7,8,9,10,11,12,13,',',1,2,3,4,5,6,7,8,9,10,11,12,13,',',1,2,3,4,5,6,7,8,9,10,11,12,13,',1,1,1,0,1,2,3,0),(6,3,1,NULL,NULL,'ps4','ps4','','ps4','',4,0,0,0,'',5,0,100,NULL,1,'',',14,1,2,3,4,5,6,7,8,9,10,11,12,13,',',1,2,3,4,5,6,7,8,9,10,11,12,13,',',1,2,3,4,5,6,7,8,9,10,11,12,13,',1,1,1,0,1,2,3,0),(7,3,1,NULL,NULL,'xboxone','xboxone','','xboxone','',5,0,0,0,'',5,0,100,NULL,1,'',',14,1,2,3,4,5,6,7,8,9,10,11,12,',',1,2,3,4,5,6,7,8,9,10,11,12,',',1,2,3,4,5,6,7,8,9,10,11,12,',1,1,1,0,1,2,3,0);

/*Table structure for table `bbs_forum_group_reply` */

DROP TABLE IF EXISTS `bbs_forum_group_reply`;

CREATE TABLE `bbs_forum_group_reply` (
  `FORUM_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  PRIMARY KEY (`FORUM_ID`,`GROUP_ID`),
  KEY `FK_BBS_FORUM_GROUP_REPLY` (`GROUP_ID`),
  CONSTRAINT `FK_BBS_FORUM_GROUP_REPLY` FOREIGN KEY (`GROUP_ID`) REFERENCES `bbs_user_group` (`GROUP_ID`),
  CONSTRAINT `FK_BBS_GROUP_FORUM_REPLY` FOREIGN KEY (`FORUM_ID`) REFERENCES `bbs_forum` (`FORUM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='回复权限';

/*Data for the table `bbs_forum_group_reply` */

/*Table structure for table `bbs_forum_group_topic` */

DROP TABLE IF EXISTS `bbs_forum_group_topic`;

CREATE TABLE `bbs_forum_group_topic` (
  `FORUM_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  PRIMARY KEY (`FORUM_ID`,`GROUP_ID`),
  KEY `FK_BBS_FORUM_GROUP_TOPIC` (`GROUP_ID`),
  CONSTRAINT `FK_BBS_FORUM_GROUP_TOPIC` FOREIGN KEY (`GROUP_ID`) REFERENCES `bbs_user_group` (`GROUP_ID`),
  CONSTRAINT `FK_BBS_GROUP_FORUM_TOPIC` FOREIGN KEY (`FORUM_ID`) REFERENCES `bbs_forum` (`FORUM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='发贴权限';

/*Data for the table `bbs_forum_group_topic` */

/*Table structure for table `bbs_forum_group_view` */

DROP TABLE IF EXISTS `bbs_forum_group_view`;

CREATE TABLE `bbs_forum_group_view` (
  `GROUP_ID` int(11) NOT NULL,
  `FORUM_ID` int(11) NOT NULL,
  PRIMARY KEY (`GROUP_ID`,`FORUM_ID`),
  KEY `FK_BBS_GROUP_FORUM_VIEW` (`FORUM_ID`),
  CONSTRAINT `FK_BBS_FORUM_GROUP_VIEW` FOREIGN KEY (`GROUP_ID`) REFERENCES `bbs_user_group` (`GROUP_ID`),
  CONSTRAINT `FK_BBS_GROUP_FORUM_VIEW` FOREIGN KEY (`FORUM_ID`) REFERENCES `bbs_forum` (`FORUM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='浏览权限';

/*Data for the table `bbs_forum_group_view` */

/*Table structure for table `bbs_forum_user` */

DROP TABLE IF EXISTS `bbs_forum_user`;

CREATE TABLE `bbs_forum_user` (
  `FORUM_ID` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`FORUM_ID`,`user_id`),
  KEY `FK_BBS_FORUM_TO_USER` (`user_id`),
  CONSTRAINT `FK_BBS_FORUM_TO_USER` FOREIGN KEY (`user_id`) REFERENCES `jb_user` (`user_id`),
  CONSTRAINT `FK_BBS_USER_TO_FORUM` FOREIGN KEY (`FORUM_ID`) REFERENCES `bbs_forum` (`FORUM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='版块版主';

/*Data for the table `bbs_forum_user` */

/*Table structure for table `bbs_grade` */

DROP TABLE IF EXISTS `bbs_grade`;

CREATE TABLE `bbs_grade` (
  `GRADE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `POST_ID` int(11) NOT NULL,
  `SCORE` int(11) DEFAULT NULL,
  `REASON` varchar(100) DEFAULT NULL,
  `GRADE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`GRADE_ID`),
  KEY `FK_MEMBER_GRADE` (`user_id`),
  KEY `FK_POST_GRADE` (`POST_ID`),
  CONSTRAINT `FK_MEMBER_GRADE` FOREIGN KEY (`user_id`) REFERENCES `jb_user` (`user_id`),
  CONSTRAINT `FK_POST_GRADE` FOREIGN KEY (`POST_ID`) REFERENCES `bbs_post` (`POST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `bbs_grade` */

/*Table structure for table `bbs_group_type` */

DROP TABLE IF EXISTS `bbs_group_type`;

CREATE TABLE `bbs_group_type` (
  `GROUP_ID` int(11) NOT NULL DEFAULT '0',
  `TYPE_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`TYPE_ID`,`GROUP_ID`),
  KEY `FK_BBS_GROUP_TYPE_GROUP` (`GROUP_ID`),
  CONSTRAINT `FK_BBS_GROUP_TYPE_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `bbs_user_group` (`GROUP_ID`),
  CONSTRAINT `FK_BBS_GROUP_TYPE_TYPE` FOREIGN KEY (`TYPE_ID`) REFERENCES `bbs_post_type` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员组投票分类关联表';

/*Data for the table `bbs_group_type` */

/*Table structure for table `bbs_limit` */

DROP TABLE IF EXISTS `bbs_limit`;

CREATE TABLE `bbs_limit` (
  `limit_id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) DEFAULT '' COMMENT '限制ip',
  `user_id` int(11) DEFAULT NULL COMMENT '限制用户ID',
  PRIMARY KEY (`limit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='限制发帖回帖';

/*Data for the table `bbs_limit` */

/*Table structure for table `bbs_login_log` */

DROP TABLE IF EXISTS `bbs_login_log`;

CREATE TABLE `bbs_login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '登录用户',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `logout_time` datetime DEFAULT NULL COMMENT '退出时间',
  `ip` varchar(255) CHARACTER SET gbk DEFAULT NULL COMMENT '登录ip',
  PRIMARY KEY (`id`),
  KEY `fk_bbs_login_log_user` (`user_id`),
  CONSTRAINT `fk_bbs_login_log_user` FOREIGN KEY (`user_id`) REFERENCES `jb_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=902 DEFAULT CHARSET=utf8 COMMENT='登录日志';

/*Data for the table `bbs_login_log` */

insert  into `bbs_login_log`(`id`,`user_id`,`login_time`,`logout_time`,`ip`) values (866,5,'2017-04-26 11:02:35',NULL,'127.0.0.1'),(867,5,'2017-04-26 11:14:29',NULL,'127.0.0.1'),(868,5,'2017-04-26 11:19:30',NULL,'0:0:0:0:0:0:0:1'),(869,29,'2017-04-26 11:26:42',NULL,'0:0:0:0:0:0:0:1'),(870,29,'2017-04-26 11:30:56',NULL,'0:0:0:0:0:0:0:1'),(871,29,'2017-04-26 11:31:44',NULL,'0:0:0:0:0:0:0:1'),(872,29,'2017-04-26 13:43:54',NULL,'0:0:0:0:0:0:0:1'),(873,5,'2017-04-26 13:45:21',NULL,'0:0:0:0:0:0:0:1'),(874,5,'2017-04-26 13:52:10',NULL,'0:0:0:0:0:0:0:1'),(875,29,'2017-04-26 13:56:22',NULL,'0:0:0:0:0:0:0:1'),(876,5,'2017-04-26 15:06:21',NULL,'0:0:0:0:0:0:0:1'),(878,29,'2017-04-26 15:43:12',NULL,'0:0:0:0:0:0:0:1'),(879,29,'2017-04-26 15:53:03',NULL,'0:0:0:0:0:0:0:1'),(880,29,'2017-04-26 15:57:28',NULL,'0:0:0:0:0:0:0:1'),(881,5,'2017-04-26 15:58:04',NULL,'0:0:0:0:0:0:0:1'),(882,5,'2017-04-26 18:55:29',NULL,'113.118.235.143'),(883,5,'2017-04-26 19:09:02',NULL,'127.0.0.1'),(884,5,'2017-04-26 20:01:52',NULL,'127.0.0.1'),(885,5,'2017-04-26 22:42:13',NULL,'113.110.140.83'),(886,5,'2017-04-27 11:02:41',NULL,'113.118.235.143'),(887,5,'2017-04-27 11:18:52',NULL,'113.118.235.143'),(888,5,'2017-04-27 11:46:40',NULL,'113.118.235.143'),(889,5,'2017-04-27 12:26:43',NULL,'113.118.235.143'),(890,29,'2017-04-27 17:12:14',NULL,'113.118.234.61'),(892,29,'2017-04-27 19:14:37',NULL,'113.118.234.61'),(893,29,'2017-04-27 19:20:45',NULL,'113.118.234.61'),(895,5,'2017-04-27 21:28:43',NULL,'113.110.140.83'),(896,29,'2017-04-27 21:29:43',NULL,'14.127.25.169'),(897,29,'2017-04-28 09:37:48',NULL,'113.118.234.61'),(898,5,'2017-04-28 09:57:14',NULL,'113.118.234.61'),(899,5,'2017-04-28 10:11:30',NULL,'113.118.234.61'),(900,5,'2017-04-28 11:03:40',NULL,'113.118.234.61'),(901,5,'2017-04-28 11:31:27',NULL,'113.118.234.61');

/*Table structure for table `bbs_magic_config` */

DROP TABLE IF EXISTS `bbs_magic_config`;

CREATE TABLE `bbs_magic_config` (
  `id` int(11) NOT NULL DEFAULT '1' COMMENT '主键id',
  `magic_switch` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开启道具',
  `magic_discount` int(3) DEFAULT NULL COMMENT '道具回收折扣',
  `magic_sofa_lines` varchar(255) CHARACTER SET gbk DEFAULT NULL COMMENT '抢沙发台词',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='道具配置表';

/*Data for the table `bbs_magic_config` */

insert  into `bbs_magic_config`(`id`,`magic_switch`,`magic_discount`,`magic_sofa_lines`) values (1,1,80,'O(∩_∩)O哈哈~，沙发是我的啦O(∩_∩)O');

/*Table structure for table `bbs_magic_log` */

DROP TABLE IF EXISTS `bbs_magic_log`;

CREATE TABLE `bbs_magic_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `magic_id` smallint(5) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `log_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `operator` tinyint(2) DEFAULT NULL COMMENT '操作(0出售道具1使用道具 2丢弃道具 3购买道具,4赠送)',
  `price` int(11) DEFAULT NULL COMMENT '购买价格',
  `num` int(11) DEFAULT NULL COMMENT '购买数量或者赠送数量',
  `targetuid` int(11) DEFAULT '0' COMMENT '赠送目标用户',
  PRIMARY KEY (`log_id`),
  KEY `fk_magic_log_magic` (`magic_id`),
  KEY `fk_magic_log_user` (`user_id`),
  CONSTRAINT `fk_magic_log_magic` FOREIGN KEY (`magic_id`) REFERENCES `bbs_common_magic` (`magicid`),
  CONSTRAINT `fk_magic_log_user` FOREIGN KEY (`user_id`) REFERENCES `jb_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='道具记录表';

/*Data for the table `bbs_magic_log` */

insert  into `bbs_magic_log`(`log_id`,`magic_id`,`user_id`,`log_time`,`operator`,`price`,`num`,`targetuid`) values (1,6,5,'2015-02-04 14:58:21',3,10,1,NULL),(2,10,5,'2015-02-04 14:58:27',3,10,1,NULL);

/*Table structure for table `bbs_magic_usergroup` */

DROP TABLE IF EXISTS `bbs_magic_usergroup`;

CREATE TABLE `bbs_magic_usergroup` (
  `magicid` smallint(6) NOT NULL DEFAULT '0',
  `groupid` int(11) NOT NULL DEFAULT '0' COMMENT '有权限使用该道具的用户组id',
  PRIMARY KEY (`magicid`,`groupid`),
  KEY `fk_bbs_magic_usergroup_group` (`groupid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='道具组权限';

/*Data for the table `bbs_magic_usergroup` */

/*Table structure for table `bbs_magic_usergroup_to` */

DROP TABLE IF EXISTS `bbs_magic_usergroup_to`;

CREATE TABLE `bbs_magic_usergroup_to` (
  `magicid` smallint(6) NOT NULL DEFAULT '0',
  `groupid` int(11) NOT NULL DEFAULT '0' COMMENT '允许被使用的用户组id',
  PRIMARY KEY (`magicid`,`groupid`),
  KEY `fk_bbs_magic_usergroup_group` (`groupid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='允许被使用的用户组';

/*Data for the table `bbs_magic_usergroup_to` */

/*Table structure for table `bbs_member_magic` */

DROP TABLE IF EXISTS `bbs_member_magic`;

CREATE TABLE `bbs_member_magic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `magicid` smallint(6) NOT NULL DEFAULT '0' COMMENT '道具id',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '道具数量',
  PRIMARY KEY (`id`),
  KEY `fk_bbs_member_magic_user` (`uid`),
  KEY `fk_bbs_member_magic_magic` (`magicid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户道具表';

/*Data for the table `bbs_member_magic` */

insert  into `bbs_member_magic`(`id`,`uid`,`magicid`,`num`) values (1,5,6,1),(2,5,10,1);

/*Table structure for table `bbs_operation` */

DROP TABLE IF EXISTS `bbs_operation`;

CREATE TABLE `bbs_operation` (
  `OPERATOR_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` int(11) NOT NULL,
  `operater_id` int(11) NOT NULL COMMENT '操作会员',
  `REF_TYPE` char(4) NOT NULL COMMENT '引用类型',
  `REF_ID` int(11) NOT NULL DEFAULT '0' COMMENT '引用ID',
  `OPT_NAME` varchar(100) NOT NULL COMMENT '操作名称',
  `OPT_REASON` varchar(255) DEFAULT NULL COMMENT '操作原因',
  `OPT_TIME` datetime NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`OPERATOR_ID`),
  KEY `FK_BBS_OPEATTION` (`SITE_ID`),
  KEY `FK_BBS_OPERATION_USER` (`operater_id`),
  CONSTRAINT `FK_BBS_OPEATTION` FOREIGN KEY (`SITE_ID`) REFERENCES `jc_site` (`site_id`),
  CONSTRAINT `FK_BBS_OPERATION_USER` FOREIGN KEY (`operater_id`) REFERENCES `jb_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='主题、帖子操作记录';

/*Data for the table `bbs_operation` */

/*Table structure for table `bbs_permission` */

DROP TABLE IF EXISTS `bbs_permission`;

CREATE TABLE `bbs_permission` (
  `GROUP_ID` int(11) NOT NULL,
  `PERM_KEY` varchar(20) NOT NULL COMMENT '权限key',
  `PERM_VALUE` varchar(255) DEFAULT NULL COMMENT '权限value',
  KEY `FK_BBS_PERMISSION_GROUP` (`GROUP_ID`),
  CONSTRAINT `FK_BBS_PERMISSION_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `bbs_user_group` (`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='论坛权限';

/*Data for the table `bbs_permission` */

insert  into `bbs_permission`(`GROUP_ID`,`PERM_KEY`,`PERM_VALUE`) values (1,'allow_attach','true'),(1,'allow_reply','true'),(1,'allow_topic','true'),(1,'attach_max_size','0'),(1,'attach_per_day','0'),(1,'attach_type',''),(1,'edit_limit_minute','0'),(1,'favorite_count','0'),(1,'msg_count','0'),(1,'msg_interval','0'),(1,'msg_per_day','100'),(1,'post_interval','0'),(1,'post_per_day','100'),(2,'allow_attach','false'),(2,'allow_reply','true'),(2,'allow_topic','true'),(2,'attach_max_size','0'),(2,'attach_per_day','0'),(2,'attach_type',''),(2,'edit_limit_minute','0'),(2,'favorite_count','0'),(2,'msg_count','0'),(2,'msg_interval','0'),(2,'msg_per_day','0'),(2,'post_interval','0'),(2,'post_per_day','0'),(12,'allow_attach','true'),(12,'allow_reply','true'),(12,'allow_topic','true'),(12,'attach_max_size','0'),(12,'attach_per_day','0'),(12,'attach_type',''),(12,'edit_limit_minute','0'),(12,'favorite_count','0'),(12,'member_prohibit','true'),(12,'msg_count','0'),(12,'msg_interval','0'),(12,'msg_per_day','0'),(12,'post_interval','0'),(12,'post_limit','true'),(12,'post_per_day','0'),(12,'super_moderator','false'),(12,'topic_delete','true'),(12,'topic_edit','true'),(12,'topic_manage','true'),(12,'topic_shield','true'),(12,'topic_top','3'),(12,'view_ip','true');

/*Table structure for table `bbs_post` */

DROP TABLE IF EXISTS `bbs_post`;

CREATE TABLE `bbs_post` (
  `POST_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TOPIC_ID` int(11) NOT NULL COMMENT '主题',
  `SITE_ID` int(11) NOT NULL COMMENT '站点',
  `CONFIG_ID` int(11) NOT NULL,
  `EDITER_ID` int(11) DEFAULT NULL COMMENT '编辑器会员',
  `CREATER_ID` int(11) NOT NULL COMMENT '发贴会员',
  `CREATE_TIME` datetime NOT NULL COMMENT '发贴时间',
  `POSTER_IP` varchar(20) NOT NULL DEFAULT '' COMMENT '发贴IP',
  `EDIT_TIME` datetime DEFAULT NULL COMMENT '编辑时间',
  `EDITER_IP` varchar(20) DEFAULT '' COMMENT '编辑者IP',
  `EDIT_COUNT` int(11) NOT NULL DEFAULT '0' COMMENT '编辑次数',
  `INDEX_COUNT` int(11) NOT NULL DEFAULT '1' COMMENT '第几楼',
  `STATUS` smallint(6) NOT NULL DEFAULT '0' COMMENT '状态',
  `IS_AFFIX` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否上传附件',
  `IS_HIDDEN` tinyint(1) DEFAULT '0' COMMENT '是否有隐藏内容',
  `TYPE_ID` int(11) NOT NULL COMMENT '帖子分类id',
  `ANONYMOUS` tinyint(1) DEFAULT NULL COMMENT '是否匿名',
  PRIMARY KEY (`POST_ID`),
  KEY `FK_BBS_POST_CREATER` (`CREATER_ID`),
  KEY `FK_BBS_POST_EDITOR` (`EDITER_ID`),
  KEY `FK_BBS_POST_TOPIC` (`TOPIC_ID`),
  KEY `FK_BBS_POST_WEBSITE` (`SITE_ID`),
  CONSTRAINT `FK_BBS_POST_CREATER` FOREIGN KEY (`CREATER_ID`) REFERENCES `jb_user` (`user_id`),
  CONSTRAINT `FK_BBS_POST_EDITOR` FOREIGN KEY (`EDITER_ID`) REFERENCES `jb_user` (`user_id`),
  CONSTRAINT `FK_BBS_POST_TOPIC` FOREIGN KEY (`TOPIC_ID`) REFERENCES `bbs_topic` (`TOPIC_ID`),
  CONSTRAINT `FK_BBS_POST_WEBSITE` FOREIGN KEY (`SITE_ID`) REFERENCES `jc_site` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8 COMMENT='论坛帖子';

/*Data for the table `bbs_post` */

insert  into `bbs_post`(`POST_ID`,`TOPIC_ID`,`SITE_ID`,`CONFIG_ID`,`EDITER_ID`,`CREATER_ID`,`CREATE_TIME`,`POSTER_IP`,`EDIT_TIME`,`EDITER_IP`,`EDIT_COUNT`,`INDEX_COUNT`,`STATUS`,`IS_AFFIX`,`IS_HIDDEN`,`TYPE_ID`,`ANONYMOUS`) values (188,66,1,1,NULL,29,'2017-04-26 16:05:28','0:0:0:0:0:0:0:1',NULL,NULL,0,1,0,0,0,5,0),(189,67,1,1,NULL,29,'2017-04-27 17:12:43','113.118.234.61',NULL,NULL,0,1,0,0,0,6,0),(190,68,1,1,NULL,32,'2017-04-28 10:04:13','113.118.234.61',NULL,NULL,0,1,0,1,0,7,0),(191,68,1,1,NULL,35,'2017-04-28 11:27:44','113.118.234.61',NULL,NULL,0,2,0,0,0,7,0);

/*Table structure for table `bbs_post_text` */

DROP TABLE IF EXISTS `bbs_post_text`;

CREATE TABLE `bbs_post_text` (
  `POST_ID` bigint(20) NOT NULL,
  `POST_TITLE` varchar(100) DEFAULT NULL COMMENT '帖子标题',
  `POST_CONTENT` longtext COMMENT '帖子内容',
  PRIMARY KEY (`POST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='论坛帖子内容';

/*Data for the table `bbs_post_text` */

insert  into `bbs_post_text`(`POST_ID`,`POST_TITLE`,`POST_CONTENT`) values (188,'祝贺本站试运行','[smiley=7]'),(189,'本站测试第二贴','[smiley=15]'),(190,'《天涯明月刀》江湖身份','天涯明月刀身份系统是玩家除去日常主线和帮派盟会活动之外另一项生活类玩法，每个身份都有其专属的玩法路线，今天要给大家说的是关于杀手的玩法介绍。\r\n　　杀手，简而言之就是接单杀人，不问缘由不问信息，只是单纯的以接取悬赏榜上的暗杀令。完成雇主要求的目标，而后赚取银两。这是一个将脑袋别在裤腰带上讨生活的行当。因为你当杀手即可能杀人也可能被人反杀，就是这么简单明了的快意江湖。\r\n　　游戏中按下ctrl+K，打开身份界面，当玩家成为杀手之后，身份界面会呈现出这个身份所具备的能力。与此同时玩家也就具备了在悬赏榜上接单的资格。\r\n　　如何进行暗杀?\r\n　　1、在各大主城和城镇中，玩家可以找到暗杀情报板，在这里玩家可以查看到当前的所有悬赏信息。\r\n　　情报板中分两块：暗杀令和悬赏令。\r\n　　暗杀令：由雇主发布悬赏，杀手在查看信息之后根据自己的实力选择是否接单，接单需要准备45银的手续费。这也是为了防止被人恶意霸占导致其余玩家不能正常参与。是比较合理的设定。\r\n　　同时，选择发布暗杀也可以对自己的仇家进行悬赏，让别人来进行接取。\r\n[attachment]1[/attachment]\r\n[smiley=8]'),(191,'','路过, 打酱油...');

/*Table structure for table `bbs_post_type` */

DROP TABLE IF EXISTS `bbs_post_type`;

CREATE TABLE `bbs_post_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) CHARACTER SET gbk DEFAULT NULL COMMENT '帖子分类名称',
  `priority` int(11) DEFAULT NULL COMMENT '排序',
  `site_id` int(11) DEFAULT NULL COMMENT '站点id',
  `forum_id` int(11) NOT NULL DEFAULT '0' COMMENT '板块',
  `parent_id` int(11) DEFAULT NULL COMMENT '父类id',
  PRIMARY KEY (`type_id`),
  KEY `fk_bbs_post_type_site` (`site_id`),
  KEY `fk_bbs_post_type_parent` (`parent_id`),
  KEY `fk_bbs_type_forum` (`forum_id`),
  CONSTRAINT `fk_bbs_post_type_parent` FOREIGN KEY (`parent_id`) REFERENCES `bbs_post_type` (`type_id`),
  CONSTRAINT `fk_bbs_post_type_site` FOREIGN KEY (`site_id`) REFERENCES `jc_site` (`site_id`),
  CONSTRAINT `fk_bbs_type_forum` FOREIGN KEY (`forum_id`) REFERENCES `bbs_forum` (`FORUM_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `bbs_post_type` */

insert  into `bbs_post_type`(`type_id`,`type_name`,`priority`,`site_id`,`forum_id`,`parent_id`) values (5,'英雄攻略',0,1,3,NULL),(6,'心得交流',1,1,3,NULL),(7,'英雄介绍',0,1,4,NULL),(8,'心得交流',1,1,4,NULL),(9,'游戏介绍',0,1,5,NULL),(10,'破解说明',1,1,5,NULL),(11,'游戏介绍',0,1,6,NULL),(12,'破解说明',1,1,6,NULL),(13,'游戏介绍',0,1,7,NULL),(14,'破解说明',1,1,7,NULL);

/*Table structure for table `bbs_private_msg` */

DROP TABLE IF EXISTS `bbs_private_msg`;

CREATE TABLE `bbs_private_msg` (
  `PRIVMSG_ID` bigint(20) NOT NULL,
  `TO_USER` bigint(20) NOT NULL COMMENT '收信人',
  `FROM_USER` bigint(20) NOT NULL COMMENT '发信人',
  `MSG_TYPE` smallint(6) NOT NULL DEFAULT '1' COMMENT '类型（2：已发，1：已阅，0：未阅）',
  `MSG_SUBJECT` varchar(255) DEFAULT NULL COMMENT '主题',
  `CREATE_TIME` datetime NOT NULL COMMENT '创建时间',
  `MSG_IP` varchar(20) NOT NULL DEFAULT '' COMMENT 'IP地址',
  PRIMARY KEY (`PRIVMSG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人短消息';

/*Data for the table `bbs_private_msg` */

/*Table structure for table `bbs_private_msg_text` */

DROP TABLE IF EXISTS `bbs_private_msg_text`;

CREATE TABLE `bbs_private_msg_text` (
  `PRIVMSG_ID` bigint(20) NOT NULL,
  `MSG_TEXT` longtext COMMENT '个人信息内容',
  PRIMARY KEY (`PRIVMSG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人消息内容';

/*Data for the table `bbs_private_msg_text` */

/*Table structure for table `bbs_report` */

DROP TABLE IF EXISTS `bbs_report`;

CREATE TABLE `bbs_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_url` varchar(255) CHARACTER SET gbk NOT NULL DEFAULT '' COMMENT '举报地址',
  `process_user` int(11) DEFAULT NULL COMMENT '处理人',
  `process_time` datetime DEFAULT NULL COMMENT '处理时间',
  `process_result` varchar(255) CHARACTER SET gbk DEFAULT NULL COMMENT '处理结果',
  `status` tinyint(1) DEFAULT NULL COMMENT '处理状态0未处理。1已经处理',
  `report_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '举报时间',
  PRIMARY KEY (`id`),
  KEY `fk_bbs_report_process_user` (`process_user`),
  CONSTRAINT `fk_bbs_report_process_user` FOREIGN KEY (`process_user`) REFERENCES `jb_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='举报记录';

/*Data for the table `bbs_report` */

insert  into `bbs_report`(`id`,`report_url`,`process_user`,`process_time`,`process_result`,`status`,`report_time`) values (1,'http：//123.207.57.174：8090/jsjl/33.jhtml＃pidnull',NULL,NULL,NULL,0,'2014-11-20 15:29:36');

/*Table structure for table `bbs_report_ext` */

DROP TABLE IF EXISTS `bbs_report_ext`;

CREATE TABLE `bbs_report_ext` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '举报id',
  `report_id` int(11) NOT NULL DEFAULT '0' COMMENT 'reportid',
  `report_user` int(11) NOT NULL DEFAULT '0' COMMENT '举报人',
  `report_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '举报时间',
  `report_reason` varchar(255) CHARACTER SET gbk DEFAULT NULL COMMENT '举报理由',
  PRIMARY KEY (`id`),
  KEY `fk_bbs_report_ext_report_user` (`report_user`),
  KEY `fk_bbs_report_ext_report` (`report_id`),
  CONSTRAINT `fk_bbs_report_ext_report` FOREIGN KEY (`report_id`) REFERENCES `bbs_report` (`id`),
  CONSTRAINT `fk_bbs_report_ext_report_user` FOREIGN KEY (`report_user`) REFERENCES `jb_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='举报扩展';

/*Data for the table `bbs_report_ext` */

insert  into `bbs_report_ext`(`id`,`report_id`,`report_user`,`report_time`,`report_reason`) values (1,1,5,'2014-11-20 15:29:36','<script＞alert(1)</script＞');

/*Table structure for table `bbs_session` */

DROP TABLE IF EXISTS `bbs_session`;

CREATE TABLE `bbs_session` (
  `sid` bigint(20) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(100) NOT NULL DEFAULT '' COMMENT '会话sessionID',
  `user_id` int(11) DEFAULT NULL COMMENT '会员用户ID',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT 'IP地址',
  `last_activetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '最后活动时间',
  `first_activetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '开始活动时间',
  PRIMARY KEY (`sid`),
  KEY `fk_session_user` (`user_id`),
  CONSTRAINT `fk_session_user` FOREIGN KEY (`user_id`) REFERENCES `jb_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1525 DEFAULT CHARSET=utf8 COMMENT='论坛会话';

/*Data for the table `bbs_session` */

insert  into `bbs_session`(`sid`,`session_id`,`user_id`,`ip`,`last_activetime`,`first_activetime`) values (1519,'88E14A6BB47C9216F0E0BBAE238010D0',34,'113.118.234.61','2017-04-28 11:41:47','2017-04-28 11:41:47'),(1520,'006D19FB2D7D27EA09D1FEA09A6EB09E',NULL,'111.206.36.16','2017-04-28 11:43:49','2017-04-28 11:43:49'),(1521,'9D633F54FC499D9D41A7889CDC12D92C',NULL,'115.239.212.11','2017-04-28 11:44:06','2017-04-28 11:44:06'),(1522,'7CBB6456AAC195FA57FACEF8E3C4D52C',NULL,'119.103.163.93','2017-04-28 11:48:00','2017-04-28 11:48:00'),(1523,'B1679652B29367CBF7F25B0FA990AD7A',NULL,'101.226.68.215','2017-04-28 11:48:03','2017-04-28 11:48:03'),(1524,'BF9314208C826D19BC927CF9C1812B4C',NULL,'61.151.226.16','2017-04-28 11:48:09','2017-04-28 11:48:09');

/*Table structure for table `bbs_third_account` */

DROP TABLE IF EXISTS `bbs_third_account`;

CREATE TABLE `bbs_third_account` (
  `account_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_key` varchar(255) NOT NULL DEFAULT '' COMMENT '第三方账号key',
  `username` varchar(100) NOT NULL DEFAULT '0' COMMENT '关联用户名',
  `source` varchar(10) NOT NULL DEFAULT '' COMMENT '第三方账号平台(QQ、新浪微博等)',
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='第三方登录平台账号';

/*Data for the table `bbs_third_account` */

/*Table structure for table `bbs_topic` */

DROP TABLE IF EXISTS `bbs_topic`;

CREATE TABLE `bbs_topic` (
  `TOPIC_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORUM_ID` int(11) NOT NULL COMMENT '板块',
  `LAST_POST_ID` int(11) DEFAULT NULL COMMENT '最后帖',
  `FIRST_POST_ID` int(11) DEFAULT NULL COMMENT '主题帖',
  `SITE_ID` int(11) NOT NULL COMMENT '站点',
  `CREATER_ID` int(11) NOT NULL COMMENT '发帖会员',
  `REPLYER_ID` int(11) NOT NULL COMMENT '最后回帖会员',
  `TITLE` varchar(100) NOT NULL COMMENT '标题',
  `CREATE_TIME` datetime NOT NULL COMMENT '创建时间',
  `LAST_TIME` datetime NOT NULL COMMENT '最后回帖时间',
  `SORT_TIME` datetime NOT NULL COMMENT '用于排序',
  `VIEW_COUNT` bigint(20) NOT NULL DEFAULT '0' COMMENT '浏览次数',
  `REPLY_COUNT` int(11) NOT NULL DEFAULT '0' COMMENT '恢复次数',
  `TOP_LEVEL` smallint(6) NOT NULL DEFAULT '0' COMMENT '固定级别',
  `PRIME_LEVEL` smallint(6) NOT NULL DEFAULT '0' COMMENT '精华级别',
  `STATUS` smallint(6) NOT NULL DEFAULT '0' COMMENT '状态',
  `OUTER_URL` varchar(255) DEFAULT NULL COMMENT '外部链接',
  `STYLE_BOLD` tinyint(1) NOT NULL DEFAULT '0' COMMENT '粗体',
  `STYLE_ITALIC` tinyint(1) NOT NULL DEFAULT '0' COMMENT '斜体',
  `STYLE_COLOR` char(6) DEFAULT NULL COMMENT '颜色',
  `STYLE_TIME` datetime DEFAULT NULL COMMENT '样式有效时间',
  `IS_AFFIX` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否上传附件',
  `HAVA_REPLY` longtext COMMENT '回复列表',
  `moderator_reply` tinyint(1) DEFAULT '0' COMMENT '版主是否回复',
  `TYPE_ID` int(11) NOT NULL COMMENT '主题分类id',
  `ALLAY_REPLY` tinyint(1) DEFAULT NULL COMMENT '主题是否允许回复',
  `HAS_SOFAED` tinyint(1) DEFAULT NULL COMMENT '主题是否已经被抢走沙发',
  `CATEGORY` tinyint(1) DEFAULT NULL COMMENT '帖子类型(0:普通帖;1:投票贴)',
  `TOTAL_COUNT` int(11) DEFAULT NULL COMMENT '总票数',
  `views_day` int(11) NOT NULL DEFAULT '0' COMMENT '日访问量',
  `views_week` int(11) NOT NULL DEFAULT '0' COMMENT '周访问量',
  `views_month` int(11) NOT NULL DEFAULT '0' COMMENT '月访问量',
  `replycount_day` int(11) NOT NULL DEFAULT '0' COMMENT '日回复量',
  PRIMARY KEY (`TOPIC_ID`),
  KEY `BBS_SORT_TIME` (`SORT_TIME`),
  KEY `FK_BBS_FIRST_POST` (`FIRST_POST_ID`),
  KEY `FK_BBS_LAST_POST` (`LAST_POST_ID`),
  KEY `FK_BBS_TOPIC_FORUM` (`FORUM_ID`),
  KEY `FK_BBS_TOPIC_USER_CREATE` (`CREATER_ID`),
  KEY `FK_BBS_TOPIC_USER_LAST` (`REPLYER_ID`),
  KEY `FK_BBS_TOPIC_SITE` (`SITE_ID`),
  CONSTRAINT `FK_BBS_FIRST_POST` FOREIGN KEY (`FIRST_POST_ID`) REFERENCES `bbs_post` (`POST_ID`),
  CONSTRAINT `FK_BBS_LAST_POST` FOREIGN KEY (`LAST_POST_ID`) REFERENCES `bbs_post` (`POST_ID`),
  CONSTRAINT `FK_BBS_TOPIC_FORUM` FOREIGN KEY (`FORUM_ID`) REFERENCES `bbs_forum` (`FORUM_ID`),
  CONSTRAINT `FK_BBS_TOPIC_SITE` FOREIGN KEY (`SITE_ID`) REFERENCES `jc_site` (`site_id`),
  CONSTRAINT `FK_BBS_TOPIC_USER_CREATE` FOREIGN KEY (`CREATER_ID`) REFERENCES `jb_user` (`user_id`),
  CONSTRAINT `FK_BBS_TOPIC_USER_LAST` FOREIGN KEY (`REPLYER_ID`) REFERENCES `jb_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COMMENT='论坛主题';

/*Data for the table `bbs_topic` */

insert  into `bbs_topic`(`TOPIC_ID`,`FORUM_ID`,`LAST_POST_ID`,`FIRST_POST_ID`,`SITE_ID`,`CREATER_ID`,`REPLYER_ID`,`TITLE`,`CREATE_TIME`,`LAST_TIME`,`SORT_TIME`,`VIEW_COUNT`,`REPLY_COUNT`,`TOP_LEVEL`,`PRIME_LEVEL`,`STATUS`,`OUTER_URL`,`STYLE_BOLD`,`STYLE_ITALIC`,`STYLE_COLOR`,`STYLE_TIME`,`IS_AFFIX`,`HAVA_REPLY`,`moderator_reply`,`TYPE_ID`,`ALLAY_REPLY`,`HAS_SOFAED`,`CATEGORY`,`TOTAL_COUNT`,`views_day`,`views_week`,`views_month`,`replycount_day`) values (66,3,NULL,188,1,29,29,'祝贺本站试运行','2017-04-26 16:05:28','2017-04-26 16:05:28','2017-04-26 16:05:28',35,0,0,0,0,NULL,0,0,NULL,NULL,0,',',0,5,1,0,0,NULL,20,20,20,0),(67,3,NULL,189,1,29,29,'本站测试第二贴','2017-04-27 17:12:43','2017-04-27 17:12:43','2017-04-27 17:12:43',12,0,0,0,0,NULL,0,0,NULL,NULL,0,',',0,6,1,0,0,NULL,9,9,9,0),(68,4,191,190,1,32,35,'《天涯明月刀》江湖身份','2017-04-28 10:04:13','2017-04-28 11:27:44','2017-04-28 11:27:44',12,1,0,0,0,NULL,0,0,NULL,NULL,1,',35,',0,7,1,0,0,NULL,7,7,7,1);

/*Table structure for table `bbs_topic_text` */

DROP TABLE IF EXISTS `bbs_topic_text`;

CREATE TABLE `bbs_topic_text` (
  `topic_id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL COMMENT '主题标题',
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='论坛主题内容';

/*Data for the table `bbs_topic_text` */

insert  into `bbs_topic_text`(`topic_id`,`title`) values (66,'祝贺本站试运行'),(67,'本站测试第二贴'),(68,'《天涯明月刀》江湖身份');

/*Table structure for table `bbs_user_active_level` */

DROP TABLE IF EXISTS `bbs_user_active_level`;

CREATE TABLE `bbs_user_active_level` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(255) NOT NULL DEFAULT '' COMMENT '等级名称',
  `required_hour` bigint(20) NOT NULL DEFAULT '0' COMMENT '等级需要时间(小时)',
  `level_img` varchar(255) DEFAULT NULL COMMENT '等级头像',
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='用户活跃等级';

/*Data for the table `bbs_user_active_level` */

insert  into `bbs_user_active_level`(`level_id`,`level_name`,`required_hour`,`level_img`) values (1,'1',0,'/r/cms/www/blue/bbs_forum/img/Lv_1.png'),(2,'2',20,'/r/cms/www/blue/bbs_forum/img/Lv_2.png'),(3,'3',50,'/r/cms/www/blue/bbs_forum/img/Lv_3.png');

/*Table structure for table `bbs_user_group` */

DROP TABLE IF EXISTS `bbs_user_group`;

CREATE TABLE `bbs_user_group` (
  `GROUP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `NAME` varchar(20) NOT NULL COMMENT '头衔',
  `GROUP_TYPE` smallint(6) NOT NULL DEFAULT '0' COMMENT '组类别',
  `GROUP_IMG` varchar(100) DEFAULT NULL COMMENT '图片',
  `GROUP_POINT` int(11) NOT NULL DEFAULT '0' COMMENT '升级积分',
  `IS_DEFAULT` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否默认组',
  `GRADE_NUM` int(11) DEFAULT '0' COMMENT '评分',
  `magic_packet_size` int(11) DEFAULT NULL COMMENT '用户组道具包容量',
  PRIMARY KEY (`GROUP_ID`),
  KEY `FK_BBS_GROUP_SITE` (`site_id`),
  CONSTRAINT `FK_BBS_GROUP_SITE` FOREIGN KEY (`site_id`) REFERENCES `jc_site` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='论坛会员组';

/*Data for the table `bbs_user_group` */

insert  into `bbs_user_group`(`GROUP_ID`,`site_id`,`NAME`,`GROUP_TYPE`,`GROUP_IMG`,`GROUP_POINT`,`IS_DEFAULT`,`GRADE_NUM`,`magic_packet_size`) values (1,1,'青铜I',1,'1',0,1,0,0),(2,1,'白银III',1,'2',100,0,0,100),(3,1,'白银II',1,'3',500,0,0,0),(4,1,'白银I',1,'4',1000,0,0,0),(5,1,'黄金III',1,'5',2000,0,0,0),(6,1,'黄金II',1,'6',4000,0,0,0),(7,1,'黄金I',1,'7',8000,0,0,0),(8,1,'白金III',1,'8',16000,0,0,0),(9,1,'白金II',1,'9',32000,0,0,0),(10,1,'白金I',1,'10',64000,0,0,0),(11,1,'钻石',1,'11',128000,0,0,0),(12,1,'版主',2,'21',0,0,0,0),(13,1,'VIP会员',3,'10',0,0,0,0),(14,1,'游客',0,'1',0,0,0,0);

/*Table structure for table `bbs_user_online` */

DROP TABLE IF EXISTS `bbs_user_online`;

CREATE TABLE `bbs_user_online` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `online_latest` double(10,2) DEFAULT NULL COMMENT '最后登录时长',
  `online_day` double(10,2) DEFAULT NULL COMMENT '今日在线时长',
  `online_week` double(10,2) DEFAULT NULL COMMENT '本周在线',
  `online_month` double(10,2) DEFAULT NULL COMMENT '本月在线',
  `online_year` double(10,2) DEFAULT NULL COMMENT '本年在线',
  `online_total` double(10,2) DEFAULT NULL COMMENT '总在线时长',
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_bbs_user_online_user` FOREIGN KEY (`user_id`) REFERENCES `jb_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=gbk COMMENT='用户在线时长统计';

/*Data for the table `bbs_user_online` */

insert  into `bbs_user_online`(`user_id`,`online_latest`,`online_day`,`online_week`,`online_month`,`online_year`,`online_total`) values (5,0.00,0.00,0.00,0.00,0.00,379.15),(29,0.00,0.00,0.00,0.00,0.00,0.00),(32,0.00,0.00,0.00,0.00,0.00,0.00),(34,0.00,0.00,0.00,0.00,0.00,0.00),(35,0.00,0.00,0.00,0.00,0.00,0.00);

/*Table structure for table `bbs_vote_item` */

DROP TABLE IF EXISTS `bbs_vote_item`;

CREATE TABLE `bbs_vote_item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `vote_count` int(11) NOT NULL DEFAULT '0' COMMENT '票数',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `bbs_vote_item` */

/*Table structure for table `bbs_vote_record` */

DROP TABLE IF EXISTS `bbs_vote_record`;

CREATE TABLE `bbs_vote_record` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `vote_time` datetime DEFAULT NULL COMMENT '投票时间',
  PRIMARY KEY (`record_id`),
  KEY `fk_vote_record_user` (`user_id`),
  KEY `fk_vote_record_topic` (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `bbs_vote_record` */

/*Table structure for table `jb_friendship` */

DROP TABLE IF EXISTS `jb_friendship`;

CREATE TABLE `jb_friendship` (
  `friendship_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `friend_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '好友状态(0:申请中;1:接受;2:拒绝)',
  PRIMARY KEY (`friendship_id`),
  KEY `fk_jb_friendship_friend` (`friend_id`),
  KEY `fk_jb_friendship_user` (`user_id`),
  CONSTRAINT `jb_friendship_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `jb_user` (`user_id`),
  CONSTRAINT `jb_friendship_ibfk_2` FOREIGN KEY (`friend_id`) REFERENCES `jb_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jb_friendship` */

/*Table structure for table `jb_message` */

DROP TABLE IF EXISTS `jb_message`;

CREATE TABLE `jb_message` (
  `msg_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT '0',
  `sender` int(11) DEFAULT NULL COMMENT '发送人',
  `receiver` int(11) DEFAULT '0' COMMENT '接收人',
  `content` longtext CHARACTER SET gbk NOT NULL COMMENT '内容',
  `create_time` datetime DEFAULT NULL COMMENT '发送时间',
  `is_sys` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为系统消息(0:不是;1:是)',
  `msg_type` int(2) NOT NULL DEFAULT '1' COMMENT '1消息，2留言,3打招呼',
  `is_read` tinyint(1) DEFAULT '0' COMMENT '信息状态 0未读 1已读',
  PRIMARY KEY (`msg_id`),
  KEY `fk_jb_message_user` (`user_id`),
  KEY `fk_jb_message_receiver` (`receiver`),
  KEY `fk_jb_message_sender` (`sender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jb_message` */

/*Table structure for table `jb_message_reply` */

DROP TABLE IF EXISTS `jb_message_reply`;

CREATE TABLE `jb_message_reply` (
  `reply_id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_id` int(11) NOT NULL DEFAULT '0',
  `sender` int(11) DEFAULT NULL,
  `receiver` int(11) NOT NULL DEFAULT '0',
  `content` longtext NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `is_read` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reply_id`),
  KEY `fk_jb_reply_sender` (`sender`),
  KEY `fk_jb_reply_receiver` (`receiver`),
  KEY `fk_jb_reply_msg` (`msg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jb_message_reply` */

/*Table structure for table `jb_user` */

DROP TABLE IF EXISTS `jb_user`;

CREATE TABLE `jb_user` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `register_time` datetime NOT NULL COMMENT '注册时间',
  `register_ip` varchar(50) NOT NULL DEFAULT '127.0.0.1' COMMENT '注册IP',
  `last_login_time` datetime NOT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(50) NOT NULL DEFAULT '127.0.0.1' COMMENT '最后登录IP',
  `login_count` int(11) NOT NULL DEFAULT '0' COMMENT '登录次数',
  `upload_total` bigint(20) NOT NULL DEFAULT '0' COMMENT '上传总大小',
  `upload_size` int(11) NOT NULL DEFAULT '0' COMMENT '上传大小',
  `upload_date` date DEFAULT NULL COMMENT '上传日期',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否管理员',
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否禁用',
  `PROHIBIT_POST` smallint(6) NOT NULL DEFAULT '0' COMMENT '禁言(0:不禁言;1:永久禁言;2:定期禁言)',
  `PROHIBIT_TIME` datetime DEFAULT NULL COMMENT '解禁时间',
  `GRADE_TODAY` int(11) DEFAULT '0' COMMENT '今日评分',
  `UPLOAD_TODAY` int(11) DEFAULT '0' COMMENT '今日上传',
  `POINT` bigint(20) DEFAULT '0' COMMENT '积分',
  `INTRODUCTION` varchar(255) DEFAULT NULL COMMENT '个人介绍',
  `SIGNED` varchar(255) DEFAULT NULL COMMENT '个性签名',
  `AVATAR` varchar(100) DEFAULT NULL COMMENT '个人头像',
  `AVATAR_TYPE` smallint(6) DEFAULT '0' COMMENT '头像类型',
  `TOPIC_COUNT` int(11) DEFAULT '0' COMMENT '主题总数',
  `REPLY_COUNT` int(11) DEFAULT '0' COMMENT '回复总数',
  `PRIME_COUNT` int(11) DEFAULT '0' COMMENT '精华总数',
  `POST_TODAY` int(11) DEFAULT '0' COMMENT '今日发帖',
  `LAST_POST_TIME` datetime DEFAULT NULL COMMENT '最后回帖时间',
  `PRESTIGE` bigint(20) DEFAULT '0' COMMENT '威望',
  `magic_packet_size` int(11) DEFAULT NULL COMMENT '用户道具包现有容量',
  `session_id` varchar(255) DEFAULT NULL,
  `active_level_id` int(11) DEFAULT NULL,
  `is_official` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `ak_username` (`username`),
  KEY `FK_BBS_MEMBER_MEMBERGROUP` (`group_id`),
  CONSTRAINT `FK_BBS_MEMBER_MEMBERGROUP` FOREIGN KEY (`group_id`) REFERENCES `bbs_user_group` (`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='BBS用户表';

/*Data for the table `jb_user` */

insert  into `jb_user`(`user_id`,`group_id`,`username`,`email`,`register_time`,`register_ip`,`last_login_time`,`last_login_ip`,`login_count`,`upload_total`,`upload_size`,`upload_date`,`is_admin`,`is_disabled`,`PROHIBIT_POST`,`PROHIBIT_TIME`,`GRADE_TODAY`,`UPLOAD_TODAY`,`POINT`,`INTRODUCTION`,`SIGNED`,`AVATAR`,`AVATAR_TYPE`,`TOPIC_COUNT`,`REPLY_COUNT`,`PRIME_COUNT`,`POST_TODAY`,`LAST_POST_TIME`,`PRESTIGE`,`magic_packet_size`,`session_id`,`active_level_id`,`is_official`) values (5,2,'admin',NULL,'2011-03-17 12:02:04','127.0.0.1','2017-04-28 11:31:27','113.118.234.61',954,0,0,'2011-03-17',1,0,0,NULL,NULL,0,297,NULL,'简介2222','/jeebbs4/user/images/201503/18160147tlda.jpg',0,61,115,0,176,NULL,7,0,'640A4E85B8CA78FB35ACE57FD985BBC1',3,0),(29,12,'702009189','702009189@qq.com','2017-04-26 11:00:02','127.0.0.1','2017-04-28 09:37:48','113.118.234.61',13,0,0,'2017-04-26',0,0,0,NULL,NULL,0,15,NULL,NULL,NULL,0,3,0,0,3,NULL,2,0,'28D2A59F724B8F574819F65512B99369',2,0),(32,1,'马文东','ma702009189@qq.com','2017-04-28 10:01:32','113.118.234.61','2017-04-28 10:01:32','113.118.234.61',0,0,0,'2017-04-28',0,0,0,NULL,NULL,0,5,NULL,'','03.gif',0,1,0,0,1,NULL,1,0,NULL,1,0),(34,1,'马文夕','mass@qq.com','2017-04-28 11:08:57','113.118.234.61','2017-04-28 11:08:57','113.118.234.61',0,0,0,'2017-04-28',0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,0,0,0,0,0,NULL,0,0,NULL,1,0),(35,1,'王大锤','wang@qq.com','2017-04-28 11:26:45','113.118.234.61','2017-04-28 11:26:45','113.118.234.61',0,0,0,'2017-04-28',0,0,0,NULL,NULL,0,0,NULL,NULL,NULL,0,0,1,0,1,NULL,0,0,NULL,1,0);

/*Table structure for table `jb_user_attr` */

DROP TABLE IF EXISTS `jb_user_attr`;

CREATE TABLE `jb_user_attr` (
  `user_id` int(11) NOT NULL,
  `attr_name` varchar(255) NOT NULL,
  `attr_value` varchar(255) DEFAULT NULL,
  KEY `pk_jb_attr_user` (`user_id`),
  CONSTRAINT `fk_jc_attr_user` FOREIGN KEY (`user_id`) REFERENCES `jb_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户属性表';

/*Data for the table `jb_user_attr` */

insert  into `jb_user_attr`(`user_id`,`attr_name`,`attr_value`) values (5,'tel',''),(29,'tel',''),(32,'tel',''),(34,'tel',''),(35,'tel','');

/*Table structure for table `jb_user_ext` */

DROP TABLE IF EXISTS `jb_user_ext`;

CREATE TABLE `jb_user_ext` (
  `user_id` int(11) NOT NULL,
  `realname` varchar(100) DEFAULT NULL COMMENT '真实姓名',
  `gender` tinyint(1) DEFAULT NULL COMMENT '性别',
  `avatar` varchar(100) DEFAULT NULL COMMENT '用户头像',
  `birthday` datetime DEFAULT NULL COMMENT '出生日期',
  `intro` varchar(255) DEFAULT NULL COMMENT '个人介绍',
  `comefrom` varchar(150) DEFAULT NULL COMMENT '来自',
  `qq` varchar(100) DEFAULT NULL COMMENT 'QQ',
  `msn` varchar(100) DEFAULT NULL COMMENT 'MSN',
  `phone` varchar(50) DEFAULT NULL COMMENT '电话',
  `moble` varchar(50) DEFAULT NULL COMMENT '手机',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='BBS用户扩展信息表';

/*Data for the table `jb_user_ext` */

insert  into `jb_user_ext`(`user_id`,`realname`,`gender`,`avatar`,`birthday`,`intro`,`comefrom`,`qq`,`msn`,`phone`,`moble`) values (5,'abc',NULL,NULL,NULL,'简介2222',NULL,NULL,NULL,NULL,NULL),(29,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(34,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(35,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `jc_config` */

DROP TABLE IF EXISTS `jc_config`;

CREATE TABLE `jc_config` (
  `config_id` int(11) NOT NULL,
  `context_path` varchar(20) DEFAULT '/JeeCms' COMMENT '部署路径',
  `servlet_point` varchar(20) DEFAULT NULL COMMENT 'Servlet挂载点',
  `port` int(11) DEFAULT NULL COMMENT '端口',
  `db_file_uri` varchar(50) NOT NULL DEFAULT '/dbfile.svl?n=' COMMENT '数据库附件访问地址',
  `is_upload_to_db` tinyint(1) NOT NULL DEFAULT '0' COMMENT '上传附件至数据库',
  `def_img` varchar(255) NOT NULL DEFAULT '/JeeCms/r/cms/www/default/no_picture.gif' COMMENT '图片不存在时默认图片',
  `login_url` varchar(255) NOT NULL DEFAULT '/login.jspx' COMMENT '登录地址',
  `process_url` varchar(255) DEFAULT NULL COMMENT '登录后处理地址',
  `mark_on` tinyint(1) NOT NULL DEFAULT '1' COMMENT '开启图片水印',
  `mark_width` int(11) NOT NULL DEFAULT '120' COMMENT '图片最小宽度',
  `mark_height` int(11) NOT NULL DEFAULT '120' COMMENT '图片最小高度',
  `mark_image` varchar(100) DEFAULT '/r/cms/www/watermark.png' COMMENT '图片水印',
  `mark_content` varchar(100) NOT NULL DEFAULT 'www.jeecms.com' COMMENT '文字水印内容',
  `mark_size` int(11) NOT NULL DEFAULT '20' COMMENT '文字水印大小',
  `mark_color` varchar(10) NOT NULL DEFAULT '#FF0000' COMMENT '文字水印颜色',
  `mark_alpha` int(11) NOT NULL DEFAULT '50' COMMENT '水印透明度（0-100）',
  `mark_position` int(11) NOT NULL DEFAULT '1' COMMENT '水印位置(0-5)',
  `mark_offset_x` int(11) NOT NULL DEFAULT '0' COMMENT 'x坐标偏移量',
  `mark_offset_y` int(11) NOT NULL DEFAULT '0' COMMENT 'y坐标偏移量',
  `count_clear_time` date NOT NULL DEFAULT '0000-00-00' COMMENT '用户活跃清除时间',
  `count_copy_time` datetime NOT NULL COMMENT '计数器拷贝时间',
  `download_code` varchar(32) NOT NULL DEFAULT 'jeecms' COMMENT '下载防盗链md5混淆码',
  `download_time` int(11) NOT NULL DEFAULT '12' COMMENT '下载有效时间（小时）',
  `email_host` varchar(50) DEFAULT NULL COMMENT '邮件发送服务器',
  `email_encoding` varchar(20) DEFAULT NULL COMMENT '邮件发送编码',
  `email_username` varchar(100) DEFAULT NULL COMMENT '邮箱用户名',
  `email_password` varchar(100) DEFAULT NULL COMMENT '邮箱密码',
  `email_personal` varchar(100) DEFAULT NULL COMMENT '邮箱发件人',
  `allow_suffix` varchar(255) DEFAULT NULL COMMENT '允许上传文件后缀',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS配置表';

/*Data for the table `jc_config` */

insert  into `jc_config`(`config_id`,`context_path`,`servlet_point`,`port`,`db_file_uri`,`is_upload_to_db`,`def_img`,`login_url`,`process_url`,`mark_on`,`mark_width`,`mark_height`,`mark_image`,`mark_content`,`mark_size`,`mark_color`,`mark_alpha`,`mark_position`,`mark_offset_x`,`mark_offset_y`,`count_clear_time`,`count_copy_time`,`download_code`,`download_time`,`email_host`,`email_encoding`,`email_username`,`email_password`,`email_personal`,`allow_suffix`) values (1,'/bbs',NULL,8090,'/dbfile.svl?n=',0,'/r/cms/www/no_picture.gif','/login.jspx',NULL,1,120,120,'/r/cms/www/watermark.png','www.jeecms.com',20,'#FF0000',50,1,0,0,'2017-04-28','2011-12-26 13:32:26','jeecms',12,NULL,NULL,NULL,NULL,NULL,'7z,aiff,asf,avi,bmp,csv,doc,docx,fla,flv,gif,gz,gzip,jpeg,jpg,mid,mov,mp3,mp4,mpc,mpeg,mpg,ods,odt,pdf,png,ppt,pxd,qt,ram,rar,rm,rmi,rmvb,rtf,sdc,sitd,swf,sxc,sxw,tar,tgz,tif,tiff,vsd,wav,wma,wmv,xls,xlsx,txt,xml,zip');

/*Table structure for table `jc_config_attr` */

DROP TABLE IF EXISTS `jc_config_attr`;

CREATE TABLE `jc_config_attr` (
  `config_id` bigint(20) NOT NULL DEFAULT '0',
  `attr_name` varchar(30) NOT NULL COMMENT '名称',
  `attr_value` varchar(255) DEFAULT NULL COMMENT '值',
  KEY `fk_attr_config` (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='BBS配置属性表';

/*Data for the table `jc_config_attr` */

insert  into `jc_config_attr`(`config_id`,`attr_name`,`attr_value`) values (1,'keepMinute','1'),(1,'qqEnable','false'),(1,'sinaEnable','false'),(1,'qqWeboEnable','false'),(1,'qqWeboKey',''),(1,'sinaKey',''),(1,'qqID','101194204'),(1,'qqKey','d4e1583fabe2a2db6e44bbcc8a2c24e8'),(1,'sinaID',''),(1,'qqWeboID',''),(1,'useronlinetopnum','146'),(1,'useronlinetopday','2015-3-21'),(1,'changeGroup','10'),(1,'autoChangeGroup','false'),(1,'serviceExpirationEmailNotice','true'),(1,'expirationEmailNoticeCount','3'),(1,'sso_2','http://cms.cmscms.com:81/sso/authenticate.jspx'),(1,'ssoEnable','false'),(1,'defaultActiveLevel','1');

/*Table structure for table `jc_config_item` */

DROP TABLE IF EXISTS `jc_config_item`;

CREATE TABLE `jc_config_item` (
  `modelitem_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_id` int(11) NOT NULL,
  `field` varchar(50) NOT NULL COMMENT '字段',
  `item_label` varchar(100) NOT NULL COMMENT '名称',
  `priority` int(11) NOT NULL DEFAULT '70' COMMENT '排列顺序',
  `def_value` varchar(255) DEFAULT NULL COMMENT '默认值',
  `opt_value` varchar(255) DEFAULT NULL COMMENT '可选项',
  `text_size` varchar(20) DEFAULT NULL COMMENT '长度',
  `area_rows` varchar(3) DEFAULT NULL COMMENT '文本行数',
  `area_cols` varchar(3) DEFAULT NULL COMMENT '文本列数',
  `help` varchar(255) DEFAULT NULL COMMENT '帮助信息',
  `help_position` varchar(1) DEFAULT NULL COMMENT '帮助位置',
  `data_type` int(11) NOT NULL DEFAULT '1' COMMENT '数据类型 "1":"字符串文本","2":"整型文本","3":"浮点型文本","4":"文本区","5":"日期","6":"下拉列表","7":"复选框","8":"单选框"',
  `is_required` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否必填',
  `category` int(11) NOT NULL DEFAULT '10' COMMENT '模型项目所属分类（10用户模型）',
  PRIMARY KEY (`modelitem_id`),
  KEY `pk_jc_item_config` (`config_id`),
  CONSTRAINT `fk_jb_item_config` FOREIGN KEY (`config_id`) REFERENCES `jc_config` (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='CMS配置模型项表';

/*Data for the table `jc_config_item` */

insert  into `jc_config_item`(`modelitem_id`,`config_id`,`field`,`item_label`,`priority`,`def_value`,`opt_value`,`text_size`,`area_rows`,`area_cols`,`help`,`help_position`,`data_type`,`is_required`,`category`) values (6,1,'tel','手机号码',1,NULL,NULL,NULL,'3','30',NULL,NULL,1,0,10);

/*Table structure for table `jc_friendlink` */

DROP TABLE IF EXISTS `jc_friendlink`;

CREATE TABLE `jc_friendlink` (
  `friendlink_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `friendlinkctg_id` int(11) NOT NULL,
  `site_name` varchar(150) NOT NULL COMMENT '网站名称',
  `domain` varchar(255) NOT NULL COMMENT '网站地址',
  `logo` varchar(150) DEFAULT NULL COMMENT '图标',
  `email` varchar(100) DEFAULT NULL COMMENT '站长邮箱',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `views` int(11) NOT NULL DEFAULT '0' COMMENT '点击次数',
  `is_enabled` char(1) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `priority` int(11) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  PRIMARY KEY (`friendlink_id`),
  KEY `fk_jc_ctg_friendlink` (`friendlinkctg_id`),
  KEY `fk_jc_friendlink_site` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS友情链接';

/*Data for the table `jc_friendlink` */

/*Table structure for table `jc_friendlink_ctg` */

DROP TABLE IF EXISTS `jc_friendlink_ctg`;

CREATE TABLE `jc_friendlink_ctg` (
  `friendlinkctg_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `friendlinkctg_name` varchar(50) NOT NULL COMMENT '名称',
  `priority` int(11) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  PRIMARY KEY (`friendlinkctg_id`),
  KEY `fk_jc_friendlinkctg_site` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS友情链接类别';

/*Data for the table `jc_friendlink_ctg` */

/*Table structure for table `jc_sensitivity` */

DROP TABLE IF EXISTS `jc_sensitivity`;

CREATE TABLE `jc_sensitivity` (
  `sensitivity_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) DEFAULT NULL,
  `search` varchar(255) NOT NULL COMMENT '敏感词',
  `replacement` varchar(255) NOT NULL COMMENT '替换词',
  PRIMARY KEY (`sensitivity_id`),
  KEY `fk_sensitivity_site` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS敏感词表';

/*Data for the table `jc_sensitivity` */

/*Table structure for table `jc_site` */

DROP TABLE IF EXISTS `jc_site`;

CREATE TABLE `jc_site` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_id` int(11) NOT NULL COMMENT '配置ID',
  `ftp_upload_id` int(11) DEFAULT NULL COMMENT '上传ftp',
  `domain` varchar(50) NOT NULL COMMENT '域名',
  `site_path` varchar(20) NOT NULL COMMENT '路径',
  `site_name` varchar(100) NOT NULL COMMENT '网站名称',
  `short_name` varchar(100) NOT NULL COMMENT '简短名称',
  `protocol` varchar(20) NOT NULL DEFAULT 'http://' COMMENT '协议',
  `dynamic_suffix` varchar(10) NOT NULL DEFAULT '.jhtml' COMMENT '动态页后缀',
  `static_suffix` varchar(10) NOT NULL DEFAULT '.html' COMMENT '静态页后缀',
  `static_dir` varchar(50) DEFAULT NULL COMMENT '静态页存放目录',
  `is_index_to_root` char(1) NOT NULL DEFAULT '0' COMMENT '是否使用将首页放在根目录下',
  `is_static_index` char(1) NOT NULL DEFAULT '0' COMMENT '是否静态化首页',
  `locale_admin` varchar(10) NOT NULL DEFAULT 'zh_CN' COMMENT '后台本地化',
  `locale_front` varchar(10) NOT NULL DEFAULT 'zh_CN' COMMENT '前台本地化',
  `tpl_solution` varchar(50) NOT NULL DEFAULT 'default' COMMENT '模板方案',
  `tpl_mobile_solution` varchar(50) NOT NULL DEFAULT '' COMMENT '手机访问模板方案',
  `final_step` tinyint(4) NOT NULL DEFAULT '2' COMMENT '终审级别',
  `after_check` tinyint(4) NOT NULL DEFAULT '2' COMMENT '审核后(1:不能修改删除;2:修改后退回;3:修改后不变)',
  `is_relative_path` char(1) NOT NULL DEFAULT '1' COMMENT '是否使用相对路径',
  `is_recycle_on` char(1) NOT NULL DEFAULT '1' COMMENT '是否开启回收站',
  `domain_alias` varchar(255) DEFAULT NULL COMMENT '域名别名',
  `domain_redirect` varchar(255) DEFAULT NULL COMMENT '域名重定向',
  `creditex_id` int(11) DEFAULT '1' COMMENT '积分交易规则id',
  PRIMARY KEY (`site_id`),
  UNIQUE KEY `ak_domain` (`domain`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='站点表';

/*Data for the table `jc_site` */

insert  into `jc_site`(`site_id`,`config_id`,`ftp_upload_id`,`domain`,`site_path`,`site_name`,`short_name`,`protocol`,`dynamic_suffix`,`static_suffix`,`static_dir`,`is_index_to_root`,`is_static_index`,`locale_admin`,`locale_front`,`tpl_solution`,`tpl_mobile_solution`,`final_step`,`after_check`,`is_relative_path`,`is_recycle_on`,`domain_alias`,`domain_redirect`,`creditex_id`) values (1,1,NULL,'123.207.57.174','www','襄阳人家论坛','xyrjBBS','http://','.jhtml','.html',NULL,'0','0','zh_CN','zh_CN','blue','mobile',2,2,'1','1','123.207.57.174,localhost','',1);

/*Table structure for table `jo_authentication` */

DROP TABLE IF EXISTS `jo_authentication`;

CREATE TABLE `jo_authentication` (
  `authentication_id` char(32) NOT NULL COMMENT '认证ID',
  `userid` int(11) NOT NULL COMMENT '用户ID',
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `login_time` datetime NOT NULL COMMENT '登录时间',
  `login_ip` varchar(50) NOT NULL COMMENT '登录ip',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='认证信息表';

/*Data for the table `jo_authentication` */

/*Table structure for table `jo_config` */

DROP TABLE IF EXISTS `jo_config`;

CREATE TABLE `jo_config` (
  `cfg_key` varchar(50) NOT NULL COMMENT '配置KEY',
  `cfg_value` varchar(255) DEFAULT NULL COMMENT '配置VALUE',
  PRIMARY KEY (`cfg_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='配置表';

/*Data for the table `jo_config` */

insert  into `jo_config`(`cfg_key`,`cfg_value`) values ('email_encoding','utf-8'),('email_host','smtp.qq.com'),('email_password','212'),('email_personal','jeecms'),('email_port',NULL),('email_username','11212@qq.com'),('login_error_interval','30'),('login_error_times','3'),('message_forgotpassword_subject','JEECMS会员密码找回信息'),('message_forgotpassword_text','感谢您使用JEECMS系统会员密码找回功能，请记住以下找回信息：\r\n用户ID：${uid}\r\n用户名：${username}\r\n您的新密码为：${resetPwd}\r\n请访问如下链接新密码才能生效：\r\nhttp://localhost:8080/jeebbs4/member/password_reset.jspx?uid=${uid}&key=${resetKey}\r\n'),('message_register_subject','JEECMS会员注册信息'),('message_register_text','${username}您好：\r\n欢迎您注册JEECMS系统会员\r\n请点击以下链接进行激活\r\nhttp://localhost:8080/jeebbs4/active.jspx?username=${username}&key=${activationCode}\r\n'),('message_serviceexpiration_subject','JEECMS商业服务到期'),('message_serviceexpiration_text','感谢您使用JEECMS系列产品，您的服务已到期，如需得到我司服务，您可以到电话联系售前人员购买支持服务，联系方式在官网可以查询：http://www.jeecms.com'),('message_subject','JEECMS会员密码找回信息'),('message_text','感谢您使用JEECMS系统会员密码找回功能，请记住以下找回信息：\r\n用户ID：${uid}\r\n用户名：${username}\r\n您的新密码为：${resetPwd}\r\n请访问如下链接新密码才能生效：\r\nhttp://localhost/member/password_reset.jspx?uid=${uid}&key=${resetKey}\r\n');

/*Table structure for table `jo_ftp` */

DROP TABLE IF EXISTS `jo_ftp`;

CREATE TABLE `jo_ftp` (
  `ftp_id` int(11) NOT NULL AUTO_INCREMENT,
  `ftp_name` varchar(100) NOT NULL COMMENT '名称',
  `ip` varchar(50) NOT NULL COMMENT 'IP',
  `port` int(11) NOT NULL DEFAULT '21' COMMENT '端口号',
  `username` varchar(100) DEFAULT NULL COMMENT '登录名',
  `password` varchar(100) DEFAULT NULL COMMENT '登陆密码',
  `encoding` varchar(20) NOT NULL DEFAULT 'UTF-8' COMMENT '编码',
  `timeout` int(11) DEFAULT NULL COMMENT '超时时间',
  `ftp_path` varchar(255) DEFAULT NULL COMMENT '路径',
  `url` varchar(255) NOT NULL COMMENT '访问URL',
  PRIMARY KEY (`ftp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='FTP表';

/*Data for the table `jo_ftp` */

/*Table structure for table `jo_template` */

DROP TABLE IF EXISTS `jo_template`;

CREATE TABLE `jo_template` (
  `tpl_name` varchar(150) NOT NULL COMMENT '模板名称',
  `tpl_source` longtext COMMENT '模板内容',
  `last_modified` bigint(20) NOT NULL COMMENT '最后修改时间',
  `is_directory` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否目录',
  PRIMARY KEY (`tpl_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='模板表';

/*Data for the table `jo_template` */

/*Table structure for table `jo_upload` */

DROP TABLE IF EXISTS `jo_upload`;

CREATE TABLE `jo_upload` (
  `filename` varchar(150) NOT NULL COMMENT '文件名',
  `length` int(11) NOT NULL COMMENT '文件大小(字节)',
  `last_modified` bigint(20) NOT NULL COMMENT '最后修改时间',
  `content` longblob NOT NULL COMMENT '内容',
  PRIMARY KEY (`filename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='上传附件表';

/*Data for the table `jo_upload` */

/*Table structure for table `jo_user` */

DROP TABLE IF EXISTS `jo_user`;

CREATE TABLE `jo_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `email` varchar(100) DEFAULT NULL COMMENT '电子邮箱',
  `password` char(32) NOT NULL COMMENT '密码',
  `register_time` datetime NOT NULL COMMENT '注册时间',
  `register_ip` varchar(50) NOT NULL DEFAULT '127.0.0.1' COMMENT '注册IP',
  `last_login_time` datetime NOT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(50) NOT NULL DEFAULT '127.0.0.1' COMMENT '最后登录IP',
  `login_count` int(11) NOT NULL DEFAULT '0' COMMENT '登录次数',
  `reset_key` char(32) DEFAULT NULL COMMENT '重置密码KEY',
  `reset_pwd` varchar(10) DEFAULT NULL COMMENT '重置密码VALUE',
  `activation` tinyint(1) NOT NULL DEFAULT '0' COMMENT '激活状态',
  `activation_code` char(32) DEFAULT NULL COMMENT '激活码',
  `error_time` datetime DEFAULT NULL COMMENT '登录错误时间',
  `error_count` int(11) NOT NULL DEFAULT '0' COMMENT '登录错误次数',
  `error_ip` varchar(50) DEFAULT NULL COMMENT '登录错误IP',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `ak_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COMMENT='用户表';

/*Data for the table `jo_user` */

insert  into `jo_user`(`user_id`,`username`,`email`,`password`,`register_time`,`register_ip`,`last_login_time`,`last_login_ip`,`login_count`,`reset_key`,`reset_pwd`,`activation`,`activation_code`,`error_time`,`error_count`,`error_ip`) values (5,'admin',NULL,'c790f82a4dd6965e66517cc0f9483e99','2011-03-17 12:02:04','127.0.0.1','2017-04-28 11:31:27','113.118.234.61',1704,NULL,NULL,1,NULL,NULL,0,NULL),(29,'702009189','702009189@qq.com','82595685f37129ac17363d2c8429575f','2017-04-26 11:00:02','127.0.0.1','2017-04-28 09:37:48','113.118.234.61',13,NULL,NULL,1,NULL,NULL,0,NULL),(32,'马文东','ma702009189@qq.com','82595685f37129ac17363d2c8429575f','2017-04-28 10:01:32','113.118.234.61','2017-04-28 10:01:32','113.118.234.61',0,NULL,NULL,1,NULL,NULL,0,NULL),(34,'马文夕','mass@qq.com','82595685f37129ac17363d2c8429575f','2017-04-28 11:08:57','113.118.234.61','2017-04-28 11:08:57','113.118.234.61',0,NULL,NULL,1,NULL,NULL,0,NULL),(35,'王大锤','wang@qq.com','82595685f37129ac17363d2c8429575f','2017-04-28 11:26:45','113.118.234.61','2017-04-28 11:26:45','113.118.234.61',0,NULL,NULL,1,NULL,NULL,0,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
